import React, { Component } from 'react'
import './App.css'
import MovieDetails from './movie/moviedetails';
import Modal from 'react-bootstrap4-modal';

class movieCard extends Component {
  constructor () {
    super()
    this.state = {
      showModal: false,
    }
  }
  render () {
    return (
      <div className="card" key={this.props.singleShow.id}>
      <img className="card-img-top" src={'http://image.tmdb.org/t/p/w185/' + this.props.singleShow.poster_path} alt="Card image cap" />
      <div className="card-body">
        <h5 className="card-title">{this.props.singleShow.title}</h5>
        <p></p>
        <p className="card-text">{this.props.singleShow.overview}</p>
        <p></p>
        <p className="card-text">{this.props.singleShow.release_date}</p>
      </div>
    </div>
    )
  }
}
export default movieCard
