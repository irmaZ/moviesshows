import React, { Component } from 'react'
import './App.css'
import MovieDetails from './movie/moviedetails';
import Modal from 'react-bootstrap4-modal';
import MovieCard from './movie-card';

import axios from 'axios'

class App extends Component {
  constructor () {
    super()
    this.state = {
      shows: [],
      movie: true,
      openDetails: [],
      showModal: false,
      movieTitle: '',
      movieDescription: '',
    }
    this.HandleMovies = this.HandleMovies.bind(this)
    this.HandleTvShows = this.HandleTvShows.bind(this)
    this.handleLoginKeyUp = this.keyUpHandler.bind(this)
    this.openDetails = this.openDetails.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  HandleMovies () {
    axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language=en-US&page=1')
      .then(response => this.setState({shows: response.data.results, movie: true}))
  }
  HandleTvShows () {
    axios.get('https://api.themoviedb.org/3/tv/popular?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language=en-US&page=1')
      .then(response => this.setState({shows: response.data.results,  movie: false}))
  }
  keyUpHandler (event) {
    if (event.target.value.length > 3) {
      console.log(event.target.value, this.state.movie)
      if (this.state.movie) {
      axios.get(`https://api.themoviedb.org/3/search/movie?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language=en-US&query=${event.target.value}&page=1&include_adult=false`)
        .then(response => this.setState({shows: response.data.results}))
      } else {
        axios.get(`https://api.themoviedb.org/3/search/tv?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language=en-US&query=${event.target.value}&page=1`)
          .then(response => this.setState({shows: response.data.results}))
      }
      }

  }
  getImage(path) {
    return `http://image.tmdb.org/t/p${path}`
  }
  openDetails(movie) {
  this.setState({
    showModal: true,
    movieTitle: movie.title,
    movieDescription: movie.overview,
  });
}
closeModal() {
  this.setState({ showModal: false });
}

handleMovieDetails() {
  this.openDetails();
}
  render () {
    const listItems = this.state.shows.map((singleShow, i) =>
    <div className="col-xl-4 card-movie"  onClick={() => this.openDetails(singleShow)}>
    <MovieCard singleShow={singleShow}/>
    </div>
  );
    return (
      <div className="container">
      <button onClick={this.HandleMovies}>
      Movies
</button>
<button onClick={this.HandleTvShows}>
  Tv shows
</button>
<div className="row">
<input type="text" className="col-xl-12 input-field" onKeyUp={this.handleLoginKeyUp} placeholder="Search.." />
</div>
<div className="row">

{listItems}
</div>
<Modal visible={this.state.showModal} onClickBackdrop={this.modalBackdropClicked}>
  <div className="modal-header">
    <h5 className="modal-title">{this.state.movieTitle}</h5>
  </div>
  <div className="modal-body">
    <p>{this.state.movieDescription}</p>
  </div>
  <div className="modal-footer">
    <button type="button" className="btn btn-secondary" onClick={this.closeModal}>
      Close
    </button>
  </div>
</Modal>
      </div>
    )
  }
}
export default App
