import React, { Component } from 'react';
import axios from 'axios';
import '../App.css';


class MovieDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movieDetails: [],
      movieCreators: [],
      movieCredits: [],
      moviePreview: [],
      country: [],
      genre: [],
      loading: true
    };
  }

  handleMovieDetails() {
    this.openDetails();
  }

  componentDidMount() {
    var userLang = navigator.language || navigator.userLanguage;

    //load movie details, credits and videos ref from TheMovieDB API
    axios
      .all([
        axios.get(
          'https://api.themoviedb.org/3/movie/' +
            this.props.id +
            '?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language='
        ),
        axios.get(
          'https://api.themoviedb.org/3/movie/' +
            this.props.id +
            '/credits?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language='
        ),
        axios.get(
          'https://api.themoviedb.org/3/movie/' +
            this.props.id +
            '/videos?api_key=90f99cff15d444dc64a9bc4f7a4fb4a1&language='
        )
      ])
      .then(
        axios.spread((movieDetails, movieCredits, moviePreviews) => {
          this.setState({
            movieDetails: movieDetails.data,
            movieCreators: movieDetails.data.created_by,
            genre: movieDetails.data.genres[0],
            movieCredits: movieCredits.data.cast,
            country: movieDetails.data.production_countries[0],
            moviePreview: moviePreviews.data.results[0],
            loading: false
          });
        })
      );
  }

  render() {
    var bg = '';

    if (
      this.state.movieDetails.backdrop_path === undefined ||
      this.state.movieDetails.backdrop_path === null
    ) {
      bg = '#333';
    } else {
      bg =
        'url(https://image.tmdb.org/t/p/w500/' +
        this.state.movieDetails.backdrop_path +
        ')';
    }

    return (
      <div className="movieDetailsContainer">
        <div className="movieDetails" style={{ background: bg }} />
        <div className="movieDetailsOverlay">
          {this.state.loading ? (
            <div>Loading...</div>
          ) : (
            <div>
                  <div className="movieTitle">
                    <span className="movieName">
                      {this.state.movieDetails.title}
                    </span>
                    <span className="movieDate">
                      {this.state.movieDetails.release_date}
                    </span>
                  </div>
                  <div className="movieSubtitle">
                    {this.state.country.iso_3166_1 + '  ·  '}
                    {this.state.genre.name + '  ·  '}
                    <i className="fa fa-clock-o" />
                    {' ' + this.state.movieDetails.runtime + ' min'}
                  </div>
                  <p className="movieOverview">
                    {this.state.movieDetails.overview}
                  </p>
                  {this.state.movieCredits.slice(0, 3).map((cast, i) => {
                    var avatar = '';

                    if (cast.profile_path === null) {
                      avatar = '/img/default-avatar.jpg';
                    } else {
                      avatar =
                        'https://image.tmdb.org/t/p/w500/' + cast.profile_path;
                    }

                    return (
                        <div className="flex-item">
                              <div
                                style={{
                                  background: 'url(' + avatar + ') center',
                                  backgroundSize: 'cover',
                                  width: '50px',
                                  height: '50px',
                                  borderRadius: '25px'
                                }}
                              />
                        </div>
                    );
                  })}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default MovieDetails;
